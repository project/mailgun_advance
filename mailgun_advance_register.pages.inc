<?php
/**
 * Validation callback for user subscription form.
 */
function mailgun_advance_user_mailgun_advance_register_form($form, &$form_state) {
  global $user;
  $account = $form_state['build_info']['args'][0];
  $mgClient = mailgun_advance_get_client();
  if (empty($mgClient)) {
    drupal_set_message(t('Mailgun client initialization failed. update data on '.l('setting page', 'admin/config/system/mailgun_advance')), 'warning');
    return FALSE;
  }
  # Issue the call to the client.
  $result = $mgClient->get("lists/pages", array(
    'limit' => 50
  ));
  $options = array(
     '0' => 'Un-Subscribe',
     '1' => 'Subscribe for all mail(normal delivery)',
  );
  $form['sub_pre'] = array(
    '#markup' => t('Join mailing list to receive or participate in its discussion via email.')
  );  
  if (isset($result->http_response_body)) {
    $items = $result->http_response_body->items;
    if (isset($items)) {
      foreach ($items as $value) {
        $listAddress = $value->address;
        $memberAddress = $account->mail;
        $statu = NULL;
        try {
          $resultw = $mgClient->get("lists/$listAddress/members/$memberAddress");
          if (isset($resultw->http_response_body)) {
            if (isset($resultw->http_response_body->member)){
              $statu = $resultw->http_response_body->member->subscribed;
              //$mod =  $resultw->http_response_body->member->vars->moderate;
              # Issue the call to the client.
              if (isset($resultw->http_response_body->member->subscribed) && empty($resultw->http_response_body->member->subscribed)) {
                $statu = 0;
              }
            }
          }            
        } catch (Exception $e) {
          
        }
        $fallback_domain = variable_get('mailgun_advance_fallback_domain');
        if ($fallback_domain) {
          $working_domain = array($fallback_domain);
          if (filter_var($value->address, FILTER_VALIDATE_EMAIL)) {
            $explodedEmail = explode('@', $value->address);
            $domain = array_pop($explodedEmail);
            if (in_array($domain, $working_domain)) {
              $mailingemail = str_replace('.', '_', $value->address);
              $form['mailing_'.$mailingemail] = array(
                '#type' => 'radios',
                '#title' => t($value->address),
                '#options' => $options,
                '#default_value' => $statu,
              );
            }
          }
        }
        if (in_array('Administrator', $user->roles) || in_array('Siteadmin', $user->roles)) {
         /* $form['moderate_'.$value->address] = array(
            '#type' => 'checkbox',
            '#title' => t('Moderate'),
            '#default_value' => $mod,
          );*/
        }
      }
    } 
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}

function mailgun_advance_user_mailgun_advance_register_form_submit($form, $form_state) {
  global $user;
  $account = $form_state['build_info']['args'][0];
  $mgClient = mailgun_advance_get_client();
  # Issue the call to the client.
  $result = $mgClient->get("lists/pages", array(
    'limit' => 50
  ));
  if (isset($result->http_response_body)) {
    $items = $result->http_response_body->items;
    if (isset($items)) {
      foreach ($items as $value) {
        $status = NULL;
        $add = str_replace('.', '_', $value->address);
        $status = $form_state['values']['mailing_'.$add];
        $moderate = 0;
        if (isset($status)) {
          $listAddress = $value->address;
          $mail  = $account->mail;
          $name = (isset($account->realname)) ? $account->realname : $account->name;
          $subuscription = $status;
          $statu = NULL;
          try {
            $mgClient = mailgun_advance_get_client();
            $resultw = $mgClient->get("lists/$listAddress/members/$mail");
            if (isset($resultw->http_response_body)) {
              if (isset($resultw->http_response_body->member)){
                $statu = $resultw->http_response_body->member->subscribed;
                # Issue the call to the client.
               // print_r($resultw->http_response_body->member); exit;
                if (isset($resultw->http_response_body->member) && empty($resultw->http_response_body->member->subscribed)) {
                  $statu = 0;
                }
              }
            }
          } catch (Exception $e) {
          }
          if (isset($statu)) {
            $mgClient = mailgun_advance_get_client();
            $var = array('moderate' => (int)$moderate);
            if (in_array('Administrator', $user->roles) || in_array('Siteadmin', $user->roles)) {
              $mgClient->put("lists/$listAddress/members/$mail", array(
                'subscribed' => $status,
                'name'       => $name,
                'vars'        => drupal_json_encode($var)
              ));
            }
            else {
               $mgClient->put("lists/$listAddress/members/$mail", array(
                'subscribed' => $status,
                'name'       => $name,
              ));
            }
          }
          else {
            $mgClient = mailgun_advance_get_client();
            $var = array('moderate' => (int)$moderate);
            if (in_array('Administrator', $user->roles) || in_array('Siteadmin', $user->roles)) {
              $mgClient->post("lists/$listAddress/members", array(
                'address'     => $mail,
                'name'        => $name,
                'description' => '',
                'subscribed'  => $subuscription,
                'vars'        => drupal_json_encode($var)
              ));
            }
            else {
              $mgClient->post("lists/$listAddress/members", array(
                'address'     => $mail,
                'name'        => $name,
                'description' => '',
                'subscribed'  => $subuscription,
              ));            
            }
          }
        }
      }
    } 
  }
  drupal_set_message(t('Your subscription has been updated.'));
}
