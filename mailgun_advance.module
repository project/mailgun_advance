<?php

/**
 * @file
 * Provides integration with Mailgun's email sending API.
 */

/**
 * Implements hook_menu().
 */
function mailgun_advance_menu() {
  $items = array();
  $items['admin/config/system/mailgun_advance'] = array(
    'title' => t('Mailgun Advance'),
    'description' => 'Configure Mailgun settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mailgun_advance_admin_settings'),
    'access arguments' => array('administer mailgun_advance'),
    'file' => 'mailgun_advance.admin.inc',
  );
  $items['admin/config/system/mailgun_advance/settings'] = array(
    'title' => 'Settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
  );
  $items['admin/config/system/mailgun_advance/testmail'] = array(
    'title' => t('Send test email'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mailgun_advance_test_form'),
    'access arguments' => array('administer mailgun_advance'),
    'description' => 'Send a test e-mail using the Mailgun API.',
    'file' => 'mailgun_advance.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );
  $items['admin/config/system/mailgun_advance/testmailgun'] = array(
    'title' => t('Mailgun status'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mailgun_advance_status_form'),
    'access arguments' => array('administer mailgun_advance'),
    'file' => 'mailgun_advance.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 20,
  );
  $items['admin/config/system/mailgun_advance/user_mailgun_register_maillist'] = array(
    'title' => t('Mailgun Mailing List'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mailgun_advance_maillist_form'),
    'access arguments' => array('administer mailgun_advance'),
    'file' => 'mailgun_advance.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );
  $items['admin/config/system/mailgun_advance/mailgun_maillist_member_list/%'] = array(
    'title' => t('Mailgun Mailing List'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mailgun_advance_maillist_mailbase_form'),
    'access arguments' => array('administer mailgun_advance'),
    'file' => 'mailgun_advance.admin.inc',
    'weight' => 1,
  );
  $items['admin/config/system/mailgun_advance/user_mailgun_register_maillist/%/delete'] = array(
    'title' => t('Mailgun Mailing List Delete'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mailgun_advance_maillist_delete'),
    'access arguments' => array('administer mailgun_advance'),
    'file' => 'mailgun_advance.admin.inc',
    'weight' => 1,
  );
  $items['admin/config/system/mailgun_advance/mailgun_maillist_member_list'] = array(
    'title' => t('Mailing List Member'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mailgun_advance_maillist_member_list'),
    'access arguments' => array('administer mailgun_advance'),
    'file' => 'mailgun_advance.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  ); 
  $items['admin/people/user_mailgun_subscriptions'] = array(
    'title' => t('Mailing List Member'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mailgun_advance_maillist_member_list_advance'),
    'access arguments' => array('administer mailgun_advance'),
    'file' => 'mailgun_advance.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  ); 
  $items['user/%user/user_mailgun_advance_register'] = array(
      'title' => t('Mailing Lists'),
      'type' => MENU_LOCAL_TASK,
      'page callback' => 'drupal_get_form',
      'page arguments' => array('mailgun_advance_user_mailgun_advance_register_form', 1),
      'file' => 'mailgun_advance_register.pages.inc',
      'access arguments' => array('access content'),
      'weight' => 10,
    );
  return $items;
}

/**
 * Implements hook_permission().
 */
function mailgun_advance_permission() {
  return array(
    'administer mailgun_advance' => array(
      'title' => t('Administer Mailgun Advance'),
      'description' => t('Perform administration tasks for the Mailgun e-mail sending service.'),
      "restrict access" => TRUE,
    ),
  );
}

/**
 * Implements hook_help().
 */
function mailgun_advance_help($path, $arg) {
  switch ($path) {
    case 'admin/config/system/mailgun_advance':
      return '<p>' . t('See <a href="@url">documentation</a> for instructions on installing and configuring Mailgun.', array('@url' => url('https://www.drupal.org/node/2547591'))) . '</p>';
      break;
    case 'admin/config/system/mailgun_advance/test':
      return '<p>' . t('Use this form to send a test e-mail to ensure you have correctly configured Mailgun.') . '</p>';
      break;
  }
}

/**
 * Implements hook_cron_queue_info().
 */
function mailgun_advance_cron_queue_info() {
  $queues = array();
  $queues['mailgun_advance_queue'] = array(
    'worker callback' => 'mailgun_advance_send',
    'time' => 60,
  );
  return $queues;
}

/**
 * Implements hook_mail().
 */
function mailgun_advance_mail($key, &$message, $params) {
  switch ($key) {
    case 'test':
      $message['subject'] = t('Mailgun test email');
      $message['body'] = $params['message'];
      if ($params['attachment']) {
        $message['params']['attachments'] = array(drupal_realpath('misc/druplicon.png'));
      }
      break;
  }
}

/**
 * Get the Mailgun client to access Mailgun's endpoints.
 *
 * @param string $key
 *   The Mailgun API key. Leave empty to use the API key saved in database.
 */
function mailgun_advance_get_client($key = '') {
  $key = (empty($key)) ? variable_get('mailgun_advance_api_key', '') : $key;
  if (empty($key)) {
    watchdog('mailgun_advance', 'Mailgun client initialization failed: Missing API key.', NULL, WATCHDOG_ERROR);
    return FALSE;
  }

  $domain = variable_get('mailgun_advance_api_end_point', 'api.mailgun.net');
  $client = new \Mailgun\Mailgun($key, $domain);
  return $client;
}

/**
 * Send an e-mail using the Mailgun API.
 *
 * @param array $mailgun_message
 *   A Mailgun message array. Contains the following keys:
 *   - from: The e-mail addressthe message will be sent from.
 *   - to: The e-mail addressthe message will be sent to.
 *   - subject: The subject of the message.
 *   - text: The plain-text version of the message. Processed using check_plain().
 *   - html: The original message content. May contain HTML tags.
 *   - cc: One or more carbon copy recipients. If multiple, separate with commas.
 *   - bcc: One or more blind carbon copy recipients. If multiple, separate with commas.
 *   - o:tag: An array containing the tags to add to the message. See: https://documentation.mailgun.com/user_manual.html#tagging.
 *   - o:campaign: The campaign ID this message belongs to. See: https://documentation.mailgun.com/user_manual.html#um-campaign-analytics.
 *   - o:deliverytime: Desired time of delivery. Messages can be scheduled for a maximum of 3 days in the future. See: https://documentation.mailgun.com/api-intro.html#date-format.
 *   - o:dkim: Boolean indicating whether or not to enable DKIM signatures on per-message basis.
 *   - o:testmode: Boolean indicating whether or not to enable test mode. See: https://documentation.mailgun.com/user_manual.html#manual-testmode.
 *   - o:tracking: Boolean indicating whether or not to toggle tracking on a per-message basis. See: https://documentation.mailgun.com/user_manual.html#tracking-messages.
 *   - o:tracking-clicks: Boolean or string "htmlonly" indicating whether or not to toggle clicks tracking on a per-message basis. Has higher priority than domain-level setting.
 *   - o:tracking-opens: Boolean indicating whether or not to toggle clicks tracking on a per-message basis. Has higher priority than domain-level setting.
 *   - h:X-My-Header: h: prefix followed by an arbitrary value allows to append a custom MIME header to the message (X-My-Header in this case). For example, h:Reply-To to specify Reply-To address.
 *   - v:my-var: v: prefix followed by an arbitrary name allows to attach a custom JSON data to the message. See: https://documentation.mailgun.com/user_manual.html#manual-customdata.
 *
 * @return bool
 *   TRUE if the mail was successfully accepted, FALSE otherwise.
 */
function mailgun_advance_send($mailgun_advance_message) {
  // Use the 'from' address specified in configuration, if available.
  $basemailid = variable_get('mailgun_advance_from', '');
  if (isset($basemailid) && !empty($basemailid)) { 
    $mailgun_advance_message['from'] = $basemailid;
  } 
  // Extract the domain from the sender's email address. Use regular expression to check since it could be either a plain email address or in the form "Name <example@example.com>".
  $tokens = (preg_match('/^\s*(.+?)\s*<\s*([^>]+)\s*>$/', $mailgun_advance_message['from'], $matches) === 1) ? explode('@', $matches[2]) : explode('@', $mailgun_advance_message['from']);
  $mail_domain = array_pop($tokens);

  $client = mailgun_advance_get_client();
  if (!$client) {
    return FALSE;
  }

  // Merge the $mailgun_message array with options.
  $mailgun_advance_message += $mailgun_advance_message['params'];
  unset($mailgun_advance_message['params']);

  // Retrieve a list of available domains first.
  $domains = array();
  try {
    $result = $client->get('domains');
    if ($result->http_response_code == 200) {
      foreach ($result->http_response_body->items as $item) {
        /**
         * @Issue(
         *   "Add a configuration option that prevents from sending to sandbox
         *   domains"
         *   type="improvement"
         *   priority="low"
         * )
         * @Issue(
         *   "Log an error if there are no domains available, taking into
         *   account the 'sandbox' option"
         *   type="bug"
         *   priority="low"
         * )
         */
        if ($item->state == 'active') {
          $domains[$item->name] = $item->name;
        }
      }
    }
    else {
      watchdog('mailgun_advance', 'Mailgun server returned a %code error. Could not retrieve domain list.', array('%code' => $result->http_response_code), WATCHDOG_ERROR);
    }
  } catch (Exception $e) {
    watchdog('mailgun_advance', 'An exception occurred while retrieving domains. @code: @message', array('@code' => $e->getCode(), '@message' => $e->getMessage()), WATCHDOG_ERROR);
  }

  // Now, we need to get the working domain. This is generally the domain the
  // From address is on or the root domain of it.
  $working_domain = '';
  if (array_search($mail_domain, $domains) !== FALSE) {
    // Great. Found it.
    $working_domain = $mail_domain;
  }
  else {
    // Oops. No match. Perhaps it's a subdomain instead.
    foreach ($domains as $domain) {
      if (strpos($domain, $mail_domain) !== FALSE) {
        // Got it.
        $working_domain = $domain;
        break;
      }
    }
  }

  // If there is no matching domain, check if there is a fallback domain
  // defined.
 // if (empty($working_domain)) {
    $fallback_domain = variable_get('mailgun_advance_fallback_domain');
    if ($fallback_domain) {
      /**
       * @Issue(
       *   "Check if the fallback domain is active and log an error if not"
       *   type="bug"
       *   priority="low"
       * )
       */
      $working_domain = $fallback_domain;
    }
//  }

  // There is a chance that the user is attempting to send from an email address that's on a domain not yet added to the Mailgun account.
  // In that case, abort sending and report error.
  if (empty($working_domain)) {
    watchdog('mailgun_advance', 'Unable to locate a working domain for From address %mail. Aborting sending.', array('%mail' => $mailgun_advance_message['from']), WATCHDOG_ERROR);
    return FALSE;
  }

  try {
    if (!empty($mailgun_advance_message['attachments'])) {
      // Send message with attachments.
      $attachments = $mailgun_advance_message['attachments'];
      unset($mailgun_advance_message['attachments']);
      $result = $client->sendMessage($working_domain, $mailgun_advance_message, array('attachment' => $attachments));
    }
    else {
      // Just good old mail with no attachment.
      $result = $client->sendMessage($working_domain, $mailgun_advance_message);
    }

    // For a list of HTTP response codes, see: https://documentation.mailgun.com/api-intro.html#errors.
    if ($result->http_response_code == 200) {
      if (variable_get('mailgun_advance_log', FALSE)) {
        watchdog('mailgun_advance', 'Successfully sent message from %from to %to. %code: %message.', array('%from' => $mailgun_advance_message['from'], '%to' => $mailgun_advance_message['to'], '%code' => $result->http_response_code, '%message' => $result->http_response_body->message));
      }
      return TRUE;
    }
    else {
      watchdog('mailgun_advance', 'Failed to send message from %from to %to. %code: %message.', array('%from' => $mailgun_advance_message['from'], '%to' => $mailgun_advance_message['to'], '%code' => $result->http_response_code, '%message' => $result->http_response_body->message), WATCHDOG_ERROR);
      return FALSE;
    }
  } catch (Exception $e) {
    watchdog('mailgun_advance', 'Exception occurred while trying to send test email from %from to %to. @code: @message.', array('%from' => $mailgun_advance_message['from'], '%to' => $mailgun_advance_message['to'], '@code' => $e->getCode(), '@message' => $e->getMessage()));
  }
}
