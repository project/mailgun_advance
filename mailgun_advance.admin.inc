<?php
/**
 * @file
 * Administration page callbacks for Mailgun.
 */

/**
 * Menu callback: displays the Mailgun module settings page.
 *
 * @param array $form
 *   Form render array.
 * @param array $form_state
 *   Array containing form state values.
 *
 * @return array
 *   An array containing form items to place on the module settings page.
 */
function mailgun_advance_admin_settings($form, &$form_state) {
  $key = variable_get('mailgun_advance_api_key', '');
  $domain = variable_get('mailgun_advance_api_end_point', 'api.mailgun.net');
  $form['mailgun_advance_api_key'] = array(
    '#title' => t('Mailgun API key'),
    '#type' => 'textfield',
    '#description' => t('Get your Secret API key from the <a href="@url">Mailgun dashboard</a>.', array('@url' => url('https://mailgun.com/app/dashboard'))),
    '#default_value' => $key,
    '#required' => TRUE,
  );
  $form['mailgun_advance_api_end_point'] = array(
    '#title' => t('Mailgun API End Point'),
    '#type' => 'textfield',
    '#description' => t('Please set your API End Point without http:// or https:// , Please see details at <a href="@url">Mailgun API Page</a>.', array('@url' => url('https://github.com/mailgun/mailgun-php'))),
    '#default_value' => $domain,
    '#required' => TRUE,
  );
  $form['mailgun_advance_fallback_domain'] = array(
    '#title' => t('Fallback domain'),
    '#type' => 'textfield',
    '#description' => t('The email is normally sent using the Mailgun domain that matches the sender\'s email address. You can specify a fallback domain that will be used if there is no match. This can be useful for using a sandbox domain during development.'),
    '#required' => TRUE,
    '#default_value' => variable_get('mailgun_advance_fallback_domain', ''),
  );
  $form['mailgun_advance_from'] = array(
    '#title' => t('From Email Address'),
    '#type' => 'textfield',
    '#description' => t('The address that all emails will be sent from, optionally in the format "Name <address@example.com>". It will be used even if a sender\'s address is specified by the module sending the email.'),
    '#default_value' => variable_get('mailgun_advance_from'),
  );
  // Display settings only when a valid API key is present.
  if (!empty($key)) {
    $form['mailgun_advance_queue'] = array(
      '#title' => t('Queue mails'),
      '#type' => 'checkbox',
      '#description' => t('Mails will be queued and sent during cron runs. Useful for sending a large number of emails.'),
      '#default_value' => variable_get('mailgun_advance_queue', FALSE),
    );
    $form['mailgun_advance_log'] = array(
      '#title' => t('Log mails'),
      '#type' => 'checkbox',
      '#description' => t('Log mails sent through Mailgun. Should not be enabled on production sites. Messages fail to send will be logged regardless of this setting.'),
      '#default_value' => variable_get('mailgun_advance_log', FALSE),
    );
    $formats = array('_none' => t('- None -'));
      foreach (filter_formats() as $format) {
        if ($format->format == 'php_code') {
          continue;
        }
        $formats[$format->format] = t($format->name);
      }
    $form['mailgun_advance_format'] = array(
      '#title' => t('Text format'),
      '#type' => 'select',
      '#description' => t('Specify an additional text format to filter the message through before sending the email.'),
      '#options' => $formats,
      '#default_value' => variable_get('mailgun_advance_format', '_none'),
    );
    $form['defaults'] = array(
      '#type' => 'fieldset',
      '#title' => t('Default settings'),
      '#description' => t('These default settings apply to messages sent using Mailgun and may be overridden on a per-message basis.'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    $form['defaults']['mailgun_advance_tracking'] = array(
      '#title' => t('Enable tracking'),
      '#type' => 'select',
      '#options' => array('default' => t('Use default setting'), 'enabled' => t('Enabled'), 'disabled' => t('Disabled')),
      '#description' => t('Whether to enable event tracking by default or not. See <a href="@url">Tracking Messages</a> for details.', array('@url' => url('https://documentation.mailgun.com/user_manual.html#tracking-messages'))),
      '#default_value' => variable_get('mailgun_advance_tracking', 'default'),
    );
    $form['defaults']['mailgun_advance_tracking_clicks'] = array(
      '#title' => t('Enable click tracking'),
      '#type' => 'select',
      '#options' => array('default' => t('Use default setting'), 'enabled' => t('Enabled'), 'disabled' => t('Disabled')),
      '#description' => t('Whether to enable click tracking by default or not.'),
      '#default_value' => variable_get('mailgun_advance_tracking_clicks', 'default'),
    );
    $form['defaults']['mailgun_advance_tracking_opens'] = array(
      '#title' => t('Enable open tracking'),
      '#type' => 'select',
      '#options' => array('default' => t('Use default setting'), 'enabled' => t('Enabled'), 'disabled' => t('Disabled')),
      '#description' => t('Whether to enable open tracking by default or not.'),
      '#default_value' => variable_get('mailgun_advance_tracking_opens', 'default'),
    );
  }
  $form = system_settings_form($form);
  $form['#validate'][] = 'mailgun_advance_admin_settings_validate';
  return $form;
}

/**
 * Form validation handler for mailgun_admin_settings().
 *
 * Perform additional validation to ensure the API key entered is valid.
 */
function mailgun_advance_admin_settings_validate($form, &$form_state) {
  if ($form['mailgun_advance_api_key']['#default_value'] != $form_state['values']['mailgun_advance_api_key']) {
    // The API key has changed. Perform validation.
    $form_state['values']['mailgun_advance_api_key'] = trim($form_state['values']['mailgun_advance_api_key']);
    $client = mailgun_advance_get_client($form_state['values']['mailgun_advance_api_key']);
    try {
      $result = $client->get('domains');
      if (isset($result)) {
        drupal_set_message(t('Your API key has been successfully validated.'));
      }  
    } catch (Exception $e) {
      form_set_error('mailgun_advance_api_key', t('An exception occurred. @code: @message', array('@code' => $e->getCode(), '@message' => $e->getMessage())));
    }
  }
  variable_get('mailgun_advance_api_key', '');
}

/**
 * Form builder. Display a form for sending a test e-mail.
 */
function mailgun_advance_test_form($form, &$form_state) {
  drupal_set_title(t('Send test mail'));
  $site_email = variable_get('site_mail', '');
  $form['to'] = array(
    '#type' => 'textfield',
    '#title' => t('To'),
    '#default_value' => $site_email,
    '#description' => t('Type in an address to have the test email sent there.'),
    '#required' => TRUE,
  );
  $message = "Howdy!\n\nIf this e-mail is displayed correctly and delivered sound and safe, congrats! You have successfully configured Mailgun.";
  $message .= ' Visit the <a href="@project">project page</a> to contribute or read <a href="@documentation">documentation</a> to learn more.';
  $message = t($message, array('@project' => url('https://www.drupal.org/project/mailgun_advance'), '@documentation' => url('https://www.drupal.org/node/2547591')));
  $form['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#default_value' => $message,
    '#required' => TRUE,
  );
  $form['attachment'] = array(
    '#title' => t('Include attachment'),
    '#type' => 'checkbox',
    '#description' => t('If checked, the Drupal icon will be included as an attachment with the test e-mail.'),
    '#default_value' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );
  $form['cancel'] = array(
    '#type' => 'link',
    '#href' => 'admin/config/system/mailgun_advance',
    '#title' => t('Cancel'),
  );
  return $form;
}

/**
 * Form submission handler for mailgun_test_form().
 * Send the test e-mail.
 */
function mailgun_advance_test_form_submit($form, &$form_state) {
  $to = $form_state['values']['to'];
  $params = array(
    'message' => $form_state['values']['message'],
    'attachment' => $form_state['values']['attachment'],
  );
  $site_name = variable_get('site_name', '');
  $default_from = variable_get('site_mail', ini_get('sendmail_from'));
  $from = (!empty($site_name)) ? $site_name . ' <' . $default_from . '>' : $default_from;
  $result = drupal_mail('mailgun_advance', 'test', $to, $GLOBALS['language'], $params, $from);
  drupal_set_message(t('Test email sent from %from to %to. If you have the "Log mails" setting enabled, check the <a href="@url">database log</a> for details.', array('%from' => $result['from'], '%to' => $result['to'], '@url' => url('admin/reports/dblog'))), 'status');
}


/**
 * Form builder. Display a form for member list of mailing list.
 */
function mailgun_advance_maillist_member_list_advance($form, &$form_state) {
  $mgClient = mailgun_advance_get_client();
  if (empty($mgClient)) {
    $message = t('Mailgun client initialization failed. update data on <a href="@settings-page">setting page</a>', array(
    '@settings-page' => url('admin/config/system/mailgun_advance'),
    ));
    drupal_set_message($message, 'warning');
    return FALSE;
  }
 
  # Issue the call to the client.
  $result = $mgClient->get("lists/pages", array(
    'limit' => 10
  ));
  $form['operations'] = array(
    '#type' => 'fieldset',
    '#title' => t('Operations'),
    '#collapsible' => False, // Added
  );
  $form['operations']['action'] = array(
    '#type' => 'select',
    '#options' => array('' => t('- Choose an operation -'), '1' => t('Subscribe'), '0' => t('Un-Subscribe')),
  );  
  if (isset($result->http_response_body)) {
    $items = $result->http_response_body->items;
    if (isset($items)) {
      $header =  array('name' => t('Name'), 'address' => t('Email Address'), 'subscription' => t('Subscribed'));
      $validmailinglist = array();
      $i = 0;
      foreach ($items as $value) {
        $fallback_domain = variable_get('mailgun_advance_fallback_domain', '');
        if ($fallback_domain) {
          $working_domain = array($fallback_domain);
          if (filter_var($value->address, FILTER_VALIDATE_EMAIL)) {
            $explodedEmail = explode('@', $value->address);
            $domain = array_pop($explodedEmail);
            if (in_array($domain, $working_domain)) {
              $row = mailgun_advance_get_mailing_member_list_row($value->address);
              $mailingemail = str_replace('.', '_', $value->address);
              $form['member_list_'.$mailingemail] = array(
                '#type' => 'tableselect',
                '#options' => $row,
                '#prefix' => '<h2>Mailing list </h2>' . $value->address,
                '#header' => $header,
                '#empty' => t('No users found'),
              );
              $validmailinglist[$mailingemail] = $value->address;
              $i++;
            }
          }
        }
        else {
          $row = mailgun_advance_get_mailing_member_list_row($value->address);
          $form['member_list'][$value->address] = array(
            '#type' => 'tableselect',
            '#options' => $row,
            '#prefix' => '<h2>Mailing list </h2>' . $value->address,
            '#header' => $header,
            '#empty' => t('No users found'),
          );
          $validmailinglist[] = $value->address;
        }
      }
    } 
  }
  $form['validdomain'] = array(
    '#type' => 'hidden',
    '#value' => $validmailinglist,
  );
  $form['operations']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

function mailgun_advance_maillist_member_list_advance_submit($form, &$form_state) {
  $status = $form_state['values']['action'];
  if ($status == '') {
    drupal_set_message(t('Please select valid operation.'), 'error');
    return FALSE;
  }
  $mailinglist = $form_state['values']['validdomain'];
 // $allmailinglist = explode(' ', $mailinglist);
  $mgClient = mailgun_advance_get_client();
  foreach ($mailinglist as $key => $value ){
   // $mailingemail = str_replace('.', '_', $value);
    $data = $form_state['values']['member_list_'.$key];
    foreach ($data as $mail) {
      if(!empty($mail)) {
        $mgClient = mailgun_advance_get_client();
        $mgClient->put("lists/$value/members/$mail", array(
          'subscribed' => $status,
        ));
      }
    }
  }
  drupal_set_message(t('Subscription has been updated.'));
}

// callback mailgun_advance_get_mailing_member_list_row.
function mailgun_advance_get_mailing_member_list_row($listAddress) {
  $mgClient = mailgun_advance_get_client();
  if (empty($mgClient)) {
    $message = t('Mailgun client initialization failed. update data on <a href="@settings-page">setting page</a>', array(
    '@settings-page' => url('admin/config/system/mailgun_advance'),
    ));
    drupal_set_message($message, 'warning');
    return FALSE;
  }
  # Issue the call to the client.
  $result = $mgClient->get("lists/$listAddress/members/pages", array(
      'limit' => 100
  ));
  if (isset($result->http_response_body)) {
    $items = $result->http_response_body->items;
    if (isset($items)) {
      foreach ($items as $value) {
        $rows[$value->address] = array(
          'name' => $value->name,
          'address' => $value->address,
          'subscription' => ($value->subscribed == 1) ? t('Subscribed') : t('Unsubscribed')
        );
      }
    } 
  }
  return $rows;
}

/**
 * Form builder. Display Mailgun status and all domain list.
 */
function mailgun_advance_status_form($form, &$form_state) {
  $mgClient = mailgun_advance_get_client();
  if (empty($mgClient)) {
    $message = t('Mailgun client initialization failed. update data on <a href="@settings-page">setting page</a>', array(
    '@settings-page' => url('admin/config/system/mailgun_advance'),
    ));
    drupal_set_message($message, 'warning');
    return FALSE;
  }  
  // You could also supply your own API key like this:
  // $client = mailgun_advance_get_client('key-xxxxxxxx');
  $message = '';
  try {
    // Use the $client variable to perform any action you want. Here we're just going to get a list of domains.
    $result = $mgClient->get('domains');
    // For a list of all the response codes, see: https://documentation.mailgun.com/api-intro.html#errors
    switch ($result->http_response_code) {
      case 200:
        $message .= 'Everything worked as expected.';
        break;
      case 400:
        $message .= 'Bad Request - Often missing a required parameter.';
        break;
      case 401:
        $message .= 'Unauthorized - No valid API key provided.';
        break;
      case 402:
        $message .= 'Request Failed - Parameters were valid but request failed.';
        break;
      case 404:
        $message .= 'Not Found - The requested item doesn’t exist.';
        break;
      case 500:
      case 502:
      case 503:
      case 504:
        $message .= 'Server Errors - something is wrong on Mailgun’s end.';
        break;
    }
    $message .= "<br/>";
    $message .= $result->http_response_body->total_count . " items retrieved.<br/>";
    foreach ($result->http_response_body->items as $item) {
      $message .= 'Domain: ' . $item->name . "<br/>";
      $message .= 'Created at: ' . $item->created_at . "<br/>";
      $message .= 'State: ' . $item->state . "<br/>";
      $message .= 'Type: ' . $item->type . "<br/><br/>";
    }
  } catch (Exception $e) {
    $message .= 'Exception occurred: ' . $e->getCode() . ': ' . $e->getMessage();
  }
  $form['status'] = array(
   '#markup' => $message,
  );
  return $form;
}

/**
 * Form builder. Display Mailgun and its member list.
 */
function mailgun_advance_maillist_member_list($form, &$form_state) {
  $mgClient = mailgun_advance_get_client();
  if (empty($mgClient)) {
    $message = t('Mailgun client initialization failed. update data on <a href="@settings-page">setting page</a>', array(
    '@settings-page' => url('admin/config/system/mailgun_advance'),
    ));
    drupal_set_message($message, 'warning');
    return FALSE;
  } 
  # Issue the call to the client.
  $result = $mgClient->get("lists/pages", array(
    'limit' => 10
  ));
  if (isset($result->http_response_body)) {
    $items = $result->http_response_body->items;
    if (isset($items)) {
      foreach ($items as $value) {
        $fallback_domain = variable_get('mailgun_advance_fallback_domain', '');
        if ($fallback_domain) {
          $working_domain = array($fallback_domain);
          if (filter_var($value->address, FILTER_VALIDATE_EMAIL)) {
            $explodedEmail = explode('@', $value->address);
            $domain = array_pop($explodedEmail);
            if (in_array($domain, $working_domain)) {
              $form['member_list'][$value->address] = array(
                '#markup' => mailgun_advance_get_mailing_member_list($value->address),
                '#prefix' => '<div id="mailing_list_div">',
                '#suffix' => '</div>',
              );
            }
          }
        }
        else {
          $form['member_list'][$value->address] = array(
            '#markup' => mailgun_advance_get_mailing_member_list($value->address),
            '#prefix' => '<div id="mailing_list_div">',
            '#suffix' => '</div>',
          );
        }
      }
    } 
  }
  return $form;
}

/**
 * Form builder. Display Mailgun mailing list and form for add new mailing list.
 */
function mailgun_advance_maillist_form($form, &$form_state) {
  $mgClient = mailgun_advance_get_client();
  if (empty($mgClient)) {
    $message = t('Mailgun client initialization failed. update data on <a href="@settings-page">setting page</a>', array(
    '@settings-page' => url('admin/config/system/mailgun_advance'),
    ));
    drupal_set_message($message, 'warning');
    return FALSE;
  }
  $form['mailing_list'] = array(
    '#markup' => mailgun_advance_get_mailing_list_live(),
    '#prefix' => '<div id="mailing_list_div">',
    '#suffix' => '</div>',
  );
  $form['date_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add new Mailing List'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['date_fieldset']['address'] = array(
    '#type' => 'textfield', 
    '#title' => t('Address'), 
    '#prefix' => '<div id="">',
    '#field_suffix' => '@'.variable_get('mailgun_advance_fallback_domain', ''),
    '#size' => 60, 
    '#maxlength' => 128, 
    '#required' => TRUE,
  );
  $form['date_fieldset']['name'] = array(
    '#type' => 'textfield', 
    '#title' => t('Name'), 
    '#size' => 60, 
    '#maxlength' => 128, 
    '#required' => TRUE,
  );
  $form['date_fieldset']['description'] = array(
    '#type' => 'textfield', 
    '#title' => t('Description'), 
    '#size' => 60, 
    '#maxlength' => 128, 
    '#required' => TRUE,
  );
  $form['date_fieldset']['access_level'] = array(
    '#type' => 'select', 
    '#title' => t('Access Level'), 
    '#options' => array('readonly' => t('Read Only'), 'members' => t('Members'), 'everyone' => t('Everyone')), 
    '#required' => TRUE,
  );
  $form['date_fieldset']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
    '#suffix' => '</div>',
  );
  return $form;
}

/*
 * Callback : Add new mailing list in Mailgun submit callback
 *
 */
function mailgun_advance_maillist_form_submit(&$form, &$form_state) {
  $mgClient = mailgun_advance_get_client();
  if (empty($mgClient)) {
    $message = t('Mailgun client initialization failed. update data on <a href="@settings-page">setting page</a>', array(
    '@settings-page' => url('admin/config/system/mailgun_advance'),
    ));
    drupal_set_message($message, 'warning');
    return FALSE;
  }
  try {
    $mgClient->post("lists",array('address' => $form_state['values']['address'].'@'.variable_get('mailgun_advance_fallback_domain', ''), 'name' => $form_state['values']['name'], 'description' => $form_state['values']['description'] , 'access_level' => $form_state['values']['access_level']));
  } catch (Exception $e) {
    watchdog('mailgun_advance', 'An exception occurred while retrieving domains. @code: @message', array('@code' => $e->getCode(), '@message' => $e->getMessage()), WATCHDOG_ERROR);
  }
  $form_state['rebuild'] = TRUE;
  drupal_set_message(t('The form has been submitted.'));
  $form_state['value'] = array(); 
  $form_state['rebuild'] = TRUE;
  return $form;
}

/*
 * Get all mailing list mail and other detils.
*/
function mailgun_advance_get_mailing_list_live() {
  $mgClient = mailgun_advance_get_client();
  if (empty($mgClient)) {
    $message = t('Mailgun client initialization failed. update data on <a href="@settings-page">setting page</a>', array(
    '@settings-page' => url('admin/config/system/mailgun_advance'),
    ));
    drupal_set_message($message, 'warning');
    return FALSE;
  }  
  # Issue the call to the client.
  
  $result = $mgClient->get("lists/pages", array(
    'limit' => 10
  ));
  if (isset($result->http_response_body)) {
    $items = $result->http_response_body->items;
    if (isset($items)) {
      $header = array('Name', 'Email Address', 'Created', 'Description', 'Memeber Count', 'Access Level', 'Action');
      $rows = array();    
      foreach ($items as $value) {
        $fallback_domain = variable_get('mailgun_advance_fallback_domain', '');
        if ($fallback_domain) {
          $working_domain = array($fallback_domain);
          if (filter_var($value->address, FILTER_VALIDATE_EMAIL)) {
            $explodedEmail = explode('@', $value->address);
            $domain = array_pop($explodedEmail);
            if (in_array($domain, $working_domain)) {
              $rows[] = array($value->name, l($value->address, 'admin/config/system/mailgun_advance/mailgun_maillist_member_list/' . $value->address), $value->created_at, $value->description, $value->members_count, $value->access_level, l('List/Add Member', 'admin/config/system/mailgun_advance/mailgun_maillist_member_list/' . $value->address) . ' | ' . l('Delete Mailing list', 'admin/config/system/mailgun_advance/user_mailgun_register_maillist/' . $value->address.'/delete'));
            }
          }
        }
        else {
          $rows[] = array($value->name, l($value->address, 'admin/config/system/mailgun_advance/mailgun_maillist_member_list/' . $value->address), $value->created_at, $value->description, $value->members_count, $value->access_level, l('List/Add Member', 'admin/config/system/mailgun_advance/mailgun_maillist_member_list/' . $value->address) . ' | '. l('Delete Mailing list', 'admin/config/system/mailgun_advance/user_mailgun_register_maillist/' . $value->address . '/delete'));
        }
      }
    }
    else {
      $rows[] = array('No Mailing list Found. ' . l('click', 'admin/config/system/mailgun_advance/user_mailgun_register_maillist') . 'here to add new one.' );
    }
  }
  return '<div><div><h1>Mailing Lists</h1></div><div>' . theme('table', array('header' => $header, 'rows' => $rows)) . '</div></div>';
}

/**
 * Form builder. Delete Mailgun mailing list.
 */
function mailgun_advance_maillist_delete($form, &$form_state) {
  $account_id = arg(5);
  $form['account_id'] = array(
    '#type' => 'hidden',
    '#value' => $account_id
  );
  $form = confirm_form($form,
    'Are you sure, Do you want to delete? It will also deleted all member form this list as well.',
    'admin/config/system/mailgun_advance/user_mailgun_register_maillist',
    'The action cannot be undone.',
    'Delete',
    'Cancel'
  );
  return $form;
}

/*
 * Callback : delete mailing list and its members
 *
 */
function mailgun_advance_maillist_delete_submit($form, &$form_state) {
  // Required actions here
  drupal_Set_message($form_state['values']['account_id'] . ' mailing list have been deleted');
  $listAddress = $form_state['values']['account_id'];
  # Issue the call to the client.
  $mgClient = mailgun_advance_get_client();
  if (empty($mgClient)) {
    $message = t('Mailgun client initialization failed. update data on <a href="@settings-page">setting page</a>', array(
    '@settings-page' => url('admin/config/system/mailgun_advance'),
    ));
    drupal_set_message($message, 'warning');
    return FALSE;
  }  
  $mgClient->delete("lists/$listAddress");
  drupal_goto('admin/config/system/mailgun_advance/user_mailgun_register_maillist');
}

/**
 * Form builder. mailing list member and add new member.
 */
function mailgun_advance_maillist_mailbase_form($form, &$form_state) {
  $mailing_id = arg(5);
  $form['name'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add Member in Mailing list "' . arg(5) . '"'),
    '#collapsible' => TRUE, // Added
    '#collapsed' => TRUE,  // Added
  );
  $form['name']['author'] = array(
    '#type' => 'textfield',
    '#title' => t('Select new subscriber'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#autocomplete_path' => 'user/autocomplete',
    '#weight' => -1,
  );
  $form['name']['subuscription'] = array(
    '#type' => 'select', 
    '#title' => t('Subscribed'), 
    '#options' => array('1' => 'Yes', '0' => 'No'), 
    '#required' => TRUE,
  );
  $form['name']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
    '#submit' => array('mailgun_advance_maillist_member_form_submit'),
  );
  $form['member_list'] = array(
    '#markup' => mailgun_advance_get_mailing_member_list($mailing_id),
    '#prefix' => '<div id="mailing_list_div">',
    '#suffix' => '</div>',
  );
  return $form;
}


//submit callback for mailgun_advance_maillist_mailbase_form form
function mailgun_advance_maillist_member_form_submit($form, &$form_state) {
  $mgClient = mailgun_advance_get_client();
  if (empty($mgClient)) {
    $message = t('Mailgun client initialization failed. update data on <a href="@settings-page">setting page</a>', array(
    '@settings-page' => url('admin/config/system/mailgun_advance'),
    ));
    drupal_set_message($message, 'warning');
    return FALSE;
  }  
  $listAddress = arg(5);
  $values = $form_state['values'];
  $account = user_load_by_name($values['author']);
  # Issue the call to the client.
  $mail  = $account->mail;
  $name = $account->realname;
  $subuscription = $values['subuscription'];
  $mgClient->post("lists/$listAddress/members", array(
    'address'     => $mail,
    'name'        => $name,
    'description' => '',
    'subscribed'  => $subuscription,
  ));
}

//get all member list
function mailgun_advance_get_mailing_member_list($listAddress) {
  $mgClient = mailgun_advance_get_client();
  if (empty($mgClient)) {
    $message = t('Mailgun client initialization failed. update data on <a href="@settings-page">setting page</a>', array(
    '@settings-page' => url('admin/config/system/mailgun_advance'),
    ));
    drupal_set_message($message, 'warning');
    return FALSE;
  }  
  # Issue the call to the client.
  $result = $mgClient->get("lists/$listAddress/members/pages", array(
      'limit'      =>  100
  ));
  if (isset($result->http_response_body)) {
    $items = $result->http_response_body->items;
    if (isset($items)) {
      $header = array(t('Name'), t('Email Address'), t('Subscribed'));
      $rows = array();    
      foreach ($items as $value) {
        $rows[] = array(
        // Simple row
            $value->name, $value->address, ($value->subscribed == 1) ? t('Subscribed') : t('Unsubscribed')
        );
      }
    } 
  }
  if (empty($rows)) {
    $rows[] = array('No Mailing list Found. ' . l('click', 'admin/config/system/mailgun_advance/user_mailgun_register_maillist/' . $listAddress) . ' here to add new one.' );
  }
  return '<div><div><h2>Mailing List</h2>' . $listAddress . '</div><div>' . theme('table', array('header' => $header, 'rows' => $rows)) . '</div></div><br/>';
}
