INTRODUCTION
------------
This module provide basic functionality of mail using Mailgun like send mail 
via Mailgun and also provide the Mailgun Mailing list functionality. where you
 can create mailing lists using Mailgun Mailing List API. A mailing list is a 
 group of members (recipients) which itself has an email address, like 
 developers@mailgun.net. This address becomes an ID for this mailing list.

 
REQUIREMENTS
------------

This module requires the following modules:

 Mailsystem (https://www.drupal.org/project/mailsystem)
 Composer Manager (https://www.drupal.org/project/composer_manager)


INSTALLATION
------------

Download composer module and install it as general Drupal module is installed.

Install Mailgun PHP library using composer manage.
To install the SDK, you will need to be using Composer in your project. 
If you aren't using Composer yet, it's really simple! Here's how to install 
composer and the Mailgun SDK.

# Install Composer
curl -sS https://getcomposer.org/installer | php

# Add Mailgun as a dependency
php composer.phar require mailgun/mailgun-php:~1.8
You do also need to choose what library to use when you are sending http 
messages. Consult the php-http/client-implementation virtual package to 
find adapters to use. For more information about virtual packages please 
refer to Httplug. Example:
php composer.phar require php-http/guzzle6-adapter:^1.0

Please check Mailgun library install correctly on site at following URL:
https://www.yourdomainname.com/admin/config/system/composer-manager

Download the Mailgun Advance module and extract it in the folder 
sites/all/modules/contrib. It is recommended to place all third-party
 modules in a subfolder called contrib.

Go to the Module page at Administer > Modules(http://example.com/admin/modules)
 and enable it.
 

CONFIGURATION
-------------

Create domain on mailgun and update DNS server as per mailgun.

Install Mailgun advance module and configure it as general Drupal module is 
installed and configure it at following URL:
https://www.yourdomainname.com/admin/config/system/mailgun_advance
(please add From Email Address also)

After that you can test mail at following URL:
https://www.yourdomainname.com/admin/config/system/mailgun_advance/testmail

After update Mailgun API then update mailsystem for all outgoing mail.
https://www.yourdomainname.com/admin/config/system/mailsystem
>> change the select list with "MailgunadvanceMailSystem" option and save it 
for all select list option.

(If you need to install manually Mailgun API: I have attached the mailgun 
API zip file with other API. download vendor.zip and extract it your sites/all
 directory and update path in composer setting page to sites/all/vendor)

MAINTAINERS
-----------

Current maintainers:
 * Dilip Singh - https://www.drupal.org/user/759000
